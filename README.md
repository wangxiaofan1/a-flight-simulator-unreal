# 一个使用虚幻引擎制作的模拟飞行软件

#### 介绍
一个基于虚幻5引擎的模拟飞行软件
#### 软件架构
基于虚幻5和jsbsim开源空气动力学框架开发，可以在原有基础上进行深度开发


#### 安装教程
1.  在虚幻编辑器中打开项目
2.  生成对应平台的安装文件
3.  安装程序
4.  运行


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


